#!/usr/bin/env bash

# Remove file before execution
rm -f SampleGDALSwap/result.tif

docker run --rm \
    -v `pwd`/SampleGDALSwap:/app \
    -w /app \
    jdk-gdal-alpine \
    sh -c "javac src/com/wa/Main.java -cp /usr/share/java/gdal-3.2.0.jar \
        && ls target/classes \
        && java -cp /usr/share/java/gdal-3.2.0.jar:/app/target/classes com.wa.Main"



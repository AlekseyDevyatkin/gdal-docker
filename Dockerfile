FROM openjdk:8-jdk-alpine as builder

# Setup build env for PROJ
RUN apk add --no-cache wget curl unzip make libtool autoconf automake pkgconfig g++ sqlite sqlite-dev

# For PROJ and GDAL
RUN apk add --no-cache \
    linux-headers \
    curl-dev tiff-dev \
    zlib-dev zstd-dev \
    libjpeg-turbo-dev libpng-dev libwebp-dev openjpeg-dev \
    swig apache-ant zip python3-dev

# Download and compile PROJ
ARG PROJ_VERSION=8.0.0
# https://github.com/OSGeo/PROJ/archive/refs/tags/8.0.0.tar.gz

RUN mkdir proj \ 
    && wget -q https://github.com/OSGeo/PROJ/archive/refs/tags/${PROJ_VERSION}.tar.gz -O - \
        | tar xz -C proj --strip-components=1 \
    && cd proj \
    && ./autogen.sh \
    && ./configure --prefix=/usr --disable-static --enable-lto \
    && make -j$(nproc) \
    && make install \
    && make install DESTDIR="/build_proj" \
    && cd .. \
    && rm -rf proj \
    && for i in /build_proj/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done \
    && for i in /build_proj/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

# Download and compile GDAL
ARG GDAL_VERSION=3.2.0
# https://github.com/OSGeo/gdal/archive/refs/tags/v3.2.0.tar.gz

RUN mkdir gdal \ 
    && wget -q https://github.com/OSGeo/gdal/archive/refs/tags/v${GDAL_VERSION}.tar.gz -O - \
        | tar xz -C gdal --strip-components=1 \
    && cd gdal/gdal \
    && ./configure --prefix=/usr \
    --without-libtool --with-java="$JAVA_HOME" \
    --with-geotiff=internal --with-rename-internal-libgeotiff-symbols \
    --enable-driver-shape \
    --disable-static \
    --with-python=yes \
    --prefix="/build/usr" \
    && make -j$(nproc) BINDINGS="java python" \
    && make install BINDINGS="java python" \
    && cd ../.. \
    && rm -rf gdal

RUN mkdir -p /build_gdal_version_changing/usr/include \
    && mv /build/usr/lib                    /build_gdal_version_changing/usr \
    && mv /build/usr/include/gdal_version.h /build_gdal_version_changing/usr/include \
    && mv /build/usr/bin                    /build_gdal_version_changing/usr \
    && for i in /build_gdal_version_changing/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done \
    && for i in /build_gdal_version_changing/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done \
    && (for i in \
            # BAG driver
            /build/usr/share/gdal/bag*.xml \
            # SXF driver
            /build/usr/share/gdal/default.rsc \
            # unused
            /build/usr/share/gdal/*.svg \
            # unused
            /build/usr/share/gdal/*.png \
            # GML driver
            /build/usr/share/gdal/*.gfs \
            # GML driver
            /build/usr/share/gdal/gml_registry.xml \
            # NITF driver
            /build/usr/share/gdal/nitf* \
            # NITF driver
            /build/usr/share/gdal/gt_datum.csv \
            # NITF driver
            /build/usr/share/gdal/gt_ellips.csv \
            # PDF driver
            /build/usr/share/gdal/pdf* \
            # PDS4 driver
            /build/usr/share/gdal/pds* \
            # S57 driver
            /build/usr/share/gdal/s57* \
            # VDV driver
            /build/usr/share/gdal/vdv* \
            # DXF driver
            /build/usr/share/gdal/*.dxf \
            # DGN driver
            /build/usr/share/gdal/*.dgn \
            # OSM driver
            /build/usr/share/gdal/osm* \
            # GMLAS driver
            /build/usr/share/gdal/gmlas* \
            # PLScenes driver
            /build/usr/share/gdal/plscenes* \
            # netCDF driver
            /build/usr/share/gdal/netcdf_config.xsd \
            # PCIDSK driver
            /build/usr/share/gdal/pci* \
            # ECW and ERS drivers
            /build/usr/share/gdal/ecw_cs.wkt \
            # EEDA driver
            /build/usr/share/gdal/eedaconf.json \
            # MAP driver / ImportFromOZI()
            /build/usr/share/gdal/ozi_* \
       ;do rm $i; done)

#============================
# Runner
#============================

FROM openjdk:8-jdk-alpine as runner

RUN date

RUN apk add --no-cache \
        libstdc++ \
        sqlite-libs \
        libcurl tiff \
        zlib zstd-libs\
        libjpeg-turbo libpng openjpeg libwebp python3 \
    # libturbojpeg.so is not used by GDAL. Only libjpeg.so*
    && rm -f /usr/lib/libturbojpeg.so* \
    # Only libwebp.so is used by GDAL
    && rm -f /usr/lib/libwebpmux.so* /usr/lib/libwebpdemux.so* /usr/lib/libwebpdecoder.so*

# Order layers starting with less frequently varying ones
#COPY --from=builder  /build_thirdparty/usr/ /usr/
# COPY --from=builder  /build_proj/grids/usr/ /usr/

COPY --from=builder  /build_proj/usr/share/proj/ /usr/share/proj/
COPY --from=builder  /build_proj/usr/include/ /usr/include/
COPY --from=builder  /build_proj/usr/bin/ /usr/bin/
COPY --from=builder  /build_proj/usr/lib/ /usr/lib/

COPY --from=builder  /build/usr/share/gdal/ /usr/share/gdal/
COPY --from=builder  /build/usr/share/java/ /usr/share/java/
COPY --from=builder  /build/usr/include/ /usr/include/
COPY --from=builder  /build_gdal_version_changing/usr/ /usr/


RUN python3 -c "from osgeo.gdal import deprecation_warn"
RUN python3 -c "from osgeo import gdal"

RUN which gdal_merge.py

#============================
# GPTL integration service
#============================

# WORKDIR /app
# COPY target/gptl-integration-service-0.0.1-SNAPSHOT.jar /app/
# CMD java -jar gptl-integration-service-0.0.1-SNAPSHOT.jar

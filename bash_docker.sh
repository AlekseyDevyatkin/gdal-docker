#!/usr/bin/env bash

# Remove file before execution
rm -f SampleGDALSwap/result.tif

docker run --rm -it \
    -v `pwd`/SampleGDALSwap:/app \
    -w /app \
    jdk-gdal-alpine \
    sh


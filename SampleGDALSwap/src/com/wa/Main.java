package com.wa;

import org.gdal.gdal.Driver;
import org.gdal.gdalconst.gdalconst;
import org.gdal.gdal.gdal;
import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;

import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println(System.getProperty("java.library.path"));

        gdal.AllRegister();

        String FPATH = "kanopus_sample.tif";
        Dataset ds = gdal.Open(FPATH);
        int xSize = ds.getRasterXSize();
        int ySize = ds.getRasterYSize();
        System.out.println(ds.GetDriver().getLongName());
        System.out.println(ds.getRasterCount());

        System.out.println("Raster ds parameters:");
        System.out.println("  Projection: " + ds.GetProjectionRef());
        System.out.println("  RasterCount: " + ds.getRasterCount());
        System.out.println("  RasterSize (" + ds.getRasterXSize() + "," + ds.getRasterYSize() + ")");

        for (int iBand = 1; iBand <= ds.getRasterCount(); iBand++)
        {
            Band band = ds.GetRasterBand(iBand);
            System.out.println("Band " + iBand + " :");
            System.out.println("   DataType: " + band.getDataType());
            System.out.println("   BlockSize (" + band.GetBlockXSize() + "," + band.GetBlockYSize() + ")");
            System.out.println("   Size (" + band.getXSize() + "," + band.getYSize() + ")");
            System.out.println("   PaletteInterp: " + gdal.GetColorInterpretationName(band.GetRasterColorInterpretation()));

            for (int iOver = 0; iOver < band.GetOverviewCount(); iOver++)
            {
                Band over = band.GetOverview(iOver);
                System.out.println("      OverView " + iOver + " :");
                System.out.println("         DataType: " + over.getDataType());
                System.out.println("         Size (" + over.getXSize() + "," + over.getYSize() + ")");
                System.out.println("         PaletteInterp: " + gdal.GetColorInterpretationName(over.GetRasterColorInterpretation()));
            }
        }

        /**
         * Now we try to reorder bands
         */

        // Remove file before creation
        String newFilepath = "result.tif";
        File file = new File(newFilepath);
        file.delete();

        Driver driver = ds.GetDriver();
        // Obtain data type to create N bans on creation
        int type = ds.GetRasterBand(1).getDataType();
        Dataset newDs = driver.Create(newFilepath, xSize, ySize, 4, type);

        // Copy Spatial data
        newDs.SetSpatialRef(ds.GetSpatialRef());
        newDs.SetGeoTransform(ds.GetGeoTransform());

        if (ds.getRasterCount() < 4) {
            throw new Exception("4 bands required");
        }

        int sequence[] = {3, 2, 1, 4};
        System.out.println("Int16: " + gdalconst.GDT_Int16);
        System.out.println("Uint16: " + gdalconst.GDT_UInt16);

        for (int i = 0; i < sequence.length; i++) {
            int seqIdx = sequence[i];
            System.out.println("Processing source band " + seqIdx);
            System.out.println("Processing destination band " + (i+1));

            Band band = ds.GetRasterBand(seqIdx);
            int srcType = band.getDataType();
            System.out.println("Source Type: " + type);

            Band newBand = newDs.GetRasterBand(i+1);
            int dstType = newBand.getDataType();
            System.out.println("New band type: " + dstType);

            // Resend NODATA value
            Double noData[] = new Double[1];
            band.GetNoDataValue(noData);
            newBand.SetNoDataValue(noData[0]);

            // Set blocks
            int sizeX = band.getXSize();
            int sizeY = band.getYSize();
            // TODO - here we declare fixed type for reading, if input has another type - convertion required
            int scanline[] = new int[xSize];

            for (int y = 0; y < sizeY; y++) {
                // Use int32 for uint16 as Java types are signed
                band.ReadRaster(0, y, sizeX, 1, sizeX, 1, gdalconst.GDT_Int32, scanline);
                newBand.WriteRaster(0, y, sizeX, 1, sizeX, 1, gdalconst.GDT_Int32, scanline);
            }
        }

        gdal.GDALDestroyDriverManager();
    }
}
